//! simple example: append a track to a playlist, giving proper credit.

use std::env::{args, var};
use xspf::{Playlist, Track};

fn main() {
	let playlist_path = args().nth(1).unwrap();
	let track_location = args().nth(2).unwrap();
	let mut pl = Playlist::read_file(&playlist_path)
		.expect("unable to read playlist from file");
	// move the previous playlist identifier (and location) to the
	// attribution section.
	pl.accredit();
	// add track
	let mut track = Track::default();
	track.location.push(track_location.to_string());
	pl.track_list.push(track);
	// set the creator field to the current user
	pl.creator = Some(var("USER").expect("$USER is not set").to_string());
	// modify the playlist file in-place
	pl.write_file(&playlist_path)
		.expect("unable to write playlist to file");
}
