//! low-level library for the XSPF format
//!
//! based off of <https://xspf.org/spec>
//!
//! this library follows the pardigm of
//! "be lax with what you accept but strict with what you output",
//! and as such will accept many invalid documents as valid.
//!
//! # crate feature: parse
//! there are 3 ways of deserializing xspf documents with this library:
//! * `Playlist::read_from`
//! * `Playlist::read_file`
//! * the FromStr implemention on Playlist
//!
//! all 3 of these methods have identical logic.
//!
//! # crate feature: print
//! for serialzing xspf documents, there are a few more options.
//!
//! methods that output xml with no line breaks or indentation:
//! * `Playlist.write_to`
//! * `Playlist.write_file`
//! * `Playlist.to_string`
//!
//! corrosponding methods that output xml with line breaks, and take an
//! addtional argument (what string to indent with)
//! * `Playlist.write_to_pretty`
//! * `Playlist.to_string_pretty`
//! 
//! # crate feature: setters
//! this feature (enabled by default) defines a set of convenience methods
//! for setting various fields.
//!
//! there are two types of setters:
//! * `set_*` methods, which set a field to Some.
//! * `add_*` methods, which append to a Vec.
//!
//! all setter methods:
//! * return their reciever (so they can be chained)
//! * mutate their reciever (so call `clone()` beforehand if this is undesirable)
//!
//! setters for String fields will automatically call to_string if needed.


pub mod common;
#[cfg(feature = "parse")]
pub mod parse;
#[cfg(feature = "print")]
pub mod print;
#[cfg(feature = "setters")]
pub mod setters;

pub use crate::common::{Playlist, Track, Attribution, Meta, Extension};
